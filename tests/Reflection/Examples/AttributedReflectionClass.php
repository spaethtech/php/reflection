<?php
/**
 * @author Ryan Spaeth <rspaeth@spaethtech.com>
 * @copyright 2020 - Spaeth Technologies
 *
 * @noinspection PhpPropertyOnlyWrittenInspection
 * @noinspection PhpUnused
 * @noinspection PhpUnusedPrivateFieldInspection
 * @noinspection PhpUnusedPrivateMethodInspection
 */
declare(strict_types=1);

namespace SpaethTech\Tests\Reflection\Examples;

use SpaethTech\Reflection\ReflectionCache;
use SpaethTech\Tests\Reflection\Examples\Attributes\DeeplyInheritedPropertyAttribute;
use SpaethTech\Tests\Reflection\Examples\Attributes\InheritedMethodAttribute;
use SpaethTech\Tests\Reflection\Examples\Attributes\InheritedPropertyAttribute;
use SpaethTech\Tests\Reflection\Examples\Attributes\SimpleClassAttribute;
use SpaethTech\Tests\Reflection\Examples\Attributes\SimpleMethodAttribute;
use SpaethTech\Tests\Reflection\Examples\Attributes\SimplePropertyAttribute;
use SpaethTech\Tests\Reflection\Examples\Attributes\SimpleRepeatableAttribute;

#[SimpleClassAttribute]
#[SimpleRepeatableAttribute]
class AttributedReflectionClass
{
    use ReflectionCache;

    #[SimplePropertyAttribute]
    public string $name;

    #[SimpleRepeatableAttribute]
    #[SimpleRepeatableAttribute]
    protected string $dateOfBirth;

    #[SimplePropertyAttribute]
    #[SimpleRepeatableAttribute]
    private string $password;

    #[InheritedPropertyAttribute]
    private string $propertyWithInheritedAttribute;

    #[DeeplyInheritedPropertyAttribute]
    private string $propertyWithDeeplyInheritedAttribute;

    private string $propertyWithoutAttributes;

    #[SimpleMethodAttribute]
    public function getName(): string
    {
        return $this->name;
    }

    #[SimpleRepeatableAttribute]
    #[SimpleRepeatableAttribute]
    protected function getDateOfBirth(): string
    {
        return $this->dateOfBirth;
    }

    #[SimpleMethodAttribute]
    #[SimpleRepeatableAttribute]
    private function getPassword(): string
    {
        return $this->password;
    }

    #[InheritedMethodAttribute]
    private function methodWitInheritedAttributes(): string
    {
        return $this->propertyWithInheritedAttribute;
    }

    private function methodWithoutAttributes(): string
    {
        return $this->propertyWithoutAttributes;
    }

}
