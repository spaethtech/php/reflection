<?php

namespace SpaethTech\Tests\Reflection\Examples;

use SpaethTech\Reflection\ReflectionCache;

class SimpleReflectionClass
{
    use ReflectionCache;

    public string $name;
    protected string $dateOfBirth;
    private string $password;

    public function getName(): string
    {
        return $this->name;
    }

    protected function getDateOfBirth(): string
    {
        return $this->dateOfBirth;
    }

    private function getPassword(): string
    {
        return $this->password;
    }

}
