<?php /** @noinspection PhpUnused, PhpMultipleClassDeclarationsInspection */
declare(strict_types=1);

namespace SpaethTech\Tests\Reflection\Examples\Attributes;

use Attribute;
use SpaethTech\UCRM\SDK\Attributes\AbstractAttribute;

#[Attribute(Attribute::TARGET_ALL)]
class NotUsedAttribute extends AbstractAttribute
{
}
