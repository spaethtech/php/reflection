<?php /** @noinspection PhpUnused, PhpMultipleClassDeclarationsInspection */
declare(strict_types=1);

namespace SpaethTech\Tests\Reflection\Examples\Attributes;

use Attribute;

#[Attribute(Attribute::TARGET_PROPERTY)]
class InheritedPropertyAttribute extends SimplePropertyAttribute
{
}
