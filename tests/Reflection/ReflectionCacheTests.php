<?php
/**
 * @author Ryan Spaeth <rspaeth@spaethtech.com>
 * @copyright 2024 - Spaeth Technologies Inc.
 *
 * @noinspection PhpUnused
 * @noinspection PhpMultipleClassDeclarationsInspection
 */
declare(strict_types=1);

namespace SpaethTech\Tests\Reflection;

use PHPUnit\Framework\TestCase;
use SpaethTech\Tests\Reflection\Examples\AttributedReflectionClass;
use SpaethTech\Tests\Reflection\Examples\Attributes\SimplePropertyAttribute;

class ReflectionCacheTests extends TestCase
{

    public function testGetCachedReflectionClass() : void
    {
        $class = new AttributedReflectionClass();
        $cache = $class->getReflectionCache();

        $test = $cache->getPropertiesWithAttribute(SimplePropertyAttribute::class);
        var_dump($test);


        //$this->assertEquals($cached, $reflected);

    }

}
