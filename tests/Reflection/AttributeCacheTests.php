<?php
//declare(strict_types=1);
//
//namespace SpaethTech\UCRM\SDK\Tests\Reflection;
//
//use SpaethTech\UCRM\SDK\Tests\Reflection\Examples\AttributedReflectionClass;
//use SpaethTech\UCRM\SDK\Tests\Reflection\Examples\Attributes\NotUsedAttribute;
//use SpaethTech\UCRM\SDK\Tests\Reflection\Examples\Attributes\SimpleClassAttribute;
//use SpaethTech\UCRM\SDK\Tests\Reflection\Examples\Attributes\SimpleMethodAttribute;
//use SpaethTech\UCRM\SDK\Tests\Reflection\Examples\Attributes\SimplePropertyAttribute;
//use SpaethTech\UCRM\SDK\Tests\Reflection\Examples\Attributes\SimpleRepeatableAttribute;
//use PHPUnit\Framework\TestCase;
//
//class AttributeCacheTests extends TestCase
//{
//    //protected Generator $faker;
//
////    protected function setUp() : void
////    {
////        parent::setUp();
////        $this->faker = Factory::create();
////
////    }
//
//    public function testClassAttributes() : void
//    {
//        $attributes = AttributedReflectionClass::classAttributes();
//        $this->assertCount(2, $attributes);
//        $this->assertEquals(SimpleClassAttribute::class, $attributes[0]->getName());
//        $this->assertEquals(SimpleRepeatableAttribute::class, $attributes[1]->getName());
//
//    }
//
//    public function testClassHasAttributes() : void
//    {
//        $attributes = AttributedReflectionClass::classHasAttributes();
//        $this->assertTrue($attributes);
//    }
//
//    public function testClassHasAttribute() : void
//    {
//        $attributes = AttributedReflectionClass::classHasAttribute(SimpleClassAttribute::class);
//        $this->assertTrue($attributes);
//
//        $attributes = AttributedReflectionClass::classHasAttribute(SimpleRepeatableAttribute::class);
//        $this->assertTrue($attributes);
//    }
//
//    public function testClassAttributeInstances() : void
//    {
//        $attributes = AttributedReflectionClass::classAttributeInstances(SimpleClassAttribute::class);
//        $this->assertCount(1, $attributes);
//
//        $attributes = AttributedReflectionClass::classAttributeInstances(SimpleRepeatableAttribute::class);
//        $this->assertCount(1, $attributes);
//
//    }
//
//
//
//    public function testPropertyAttributes() : void
//    {
//        $attributes = AttributedReflectionClass::propertyAttributes("name");
//        $this->assertCount(1, $attributes);
//        $this->assertEquals(SimplePropertyAttribute::class, $attributes[0]->getName());
//
//        $attributes = AttributedReflectionClass::propertyAttributes("dateOfBirth");
//        $this->assertCount(2, $attributes);
//        $this->assertEquals(SimpleRepeatableAttribute::class, $attributes[0]->getName());
//        $this->assertEquals(SimpleRepeatableAttribute::class, $attributes[1]->getName());
//
//        $attributes = AttributedReflectionClass::propertyAttributes("password");
//        $this->assertCount(2, $attributes);
//        $this->assertEquals(SimplePropertyAttribute::class, $attributes[0]->getName());
//        $this->assertEquals(SimpleRepeatableAttribute::class, $attributes[1]->getName());
//
//        $attributes = AttributedReflectionClass::propertyAttributes("unknown");
//        $this->assertFalse($attributes);
//
//        $attributes = AttributedReflectionClass::propertyAttributes("password", SimplePropertyAttribute::class);
//        $this->assertCount(1, $attributes);
//        $this->assertEquals(SimplePropertyAttribute::class, $attributes[0]->getName());
//
//        $attributes = AttributedReflectionClass::propertyAttributes("dateOfBirth", SimpleRepeatableAttribute::class);
//        $this->assertCount(2, $attributes);
//        $this->assertEquals(SimpleRepeatableAttribute::class, $attributes[0]->getName());
//        $this->assertEquals(SimpleRepeatableAttribute::class, $attributes[1]->getName());
//
//    }
//
//    public function testPropertyHasAttributes() : void
//    {
//        $attributes = AttributedReflectionClass::propertyHasAttributes("name");
//        $this->assertTrue($attributes);
//
//        $attributes = AttributedReflectionClass::propertyHasAttributes("name");
//        $this->assertTrue($attributes);
//
//        $attributes = AttributedReflectionClass::propertyHasAttributes("propertyWithoutAttributes");
//        $this->assertFalse($attributes);
//
//        $attributes = AttributedReflectionClass::propertyHasAttributes("propertyWithoutAttributes");
//        $this->assertFalse($attributes);
//    }
//
//    public function testPropertyHasAttribute() : void
//    {
//        $attributes = AttributedReflectionClass::propertyHasAttribute("name", SimplePropertyAttribute::class);
//        $this->assertTrue($attributes);
//
//        $attributes = AttributedReflectionClass::propertyHasAttribute("dateOfBirth", SimpleRepeatableAttribute::class);
//        $this->assertTrue($attributes);
//
//        $attributes = AttributedReflectionClass::propertyHasAttribute("password", SimplePropertyAttribute::class);
//        $this->assertTrue($attributes);
//
//        $attributes = AttributedReflectionClass::propertyHasAttribute("propertyWithoutAttributes",
//            SimplePropertyAttribute::class);
//        $this->assertFalse($attributes);
//    }
//
//
//    public function testPropertiesWithAttributes() : void
//    {
//        $attributes = AttributedReflectionClass::propertiesWithAttribute(SimplePropertyAttribute::class);
//        $this->assertCount(4, $attributes);
//
//        $attributes = AttributedReflectionClass::propertiesWithAttribute(SimplePropertyAttribute::class, false);
//        $this->assertCount(2, $attributes);
//
//        $attributes = AttributedReflectionClass::propertiesWithAttribute(NotUsedAttribute::class);
//        $this->assertCount(0, $attributes);
//
//    }
//
//    public function testPropertyNamesWithAttributes() : void
//    {
//        $attributes = AttributedReflectionClass::propertyNamesWithAttribute(SimplePropertyAttribute::class);
//        $this->assertCount(4, $attributes);
//        $this->assertIsString($attributes[0]);
//
//        $attributes = AttributedReflectionClass::propertyNamesWithAttribute(SimplePropertyAttribute::class, false);
//        $this->assertCount(2, $attributes);
//        $this->assertIsString($attributes[0]);
//
//        $attributes = AttributedReflectionClass::propertyNamesWithAttribute(NotUsedAttribute::class);
//        $this->assertCount(0, $attributes);
//
//    }
//
//    public function testPropertyAttributeInstances() : void
//    {
//        $attributes = AttributedReflectionClass::propertyAttributeInstances("name", SimplePropertyAttribute::class);
//        $this->assertCount(1, $attributes);
//        $this->assertTrue($attributes[0] instanceof SimplePropertyAttribute);
//
//
//
//    }
//
//
//
//
//
//
//    public function testMethodAttributes() : void
//    {
//        $attributes = AttributedReflectionClass::methodAttributes("getName");
//        $this->assertCount(1, $attributes);
//        $this->assertEquals(SimpleMethodAttribute::class, $attributes[0]->getName());
//
//        $attributes = AttributedReflectionClass::methodAttributes("getDateOfBirth");
//        $this->assertCount(2, $attributes);
//        $this->assertEquals(SimpleRepeatableAttribute::class, $attributes[0]->getName());
//        $this->assertEquals(SimpleRepeatableAttribute::class, $attributes[1]->getName());
//
//        $attributes = AttributedReflectionClass::methodAttributes("getPassword");
//        $this->assertCount(2, $attributes);
//        $this->assertEquals(SimpleMethodAttribute::class, $attributes[0]->getName());
//        $this->assertEquals(SimpleRepeatableAttribute::class, $attributes[1]->getName());
//
//        $attributes = AttributedReflectionClass::methodAttributes("getUnknown");
//        $this->assertFalse($attributes);
//
//        $attributes = AttributedReflectionClass::methodAttributes("getPassword", SimpleMethodAttribute::class);
//        $this->assertCount(1, $attributes);
//        $this->assertEquals(SimpleMethodAttribute::class, $attributes[0]->getName());
//
//        $attributes = AttributedReflectionClass::methodAttributes("getPassword", SimpleMethodAttribute::class);
//        $this->assertCount(1, $attributes);
//        $this->assertEquals(SimpleMethodAttribute::class, $attributes[0]->getName());
//
//
//    }
//
//    public function testMethodHasAttributes() : void
//    {
//        $attributes = AttributedReflectionClass::methodHasAttributes("getName");
//        $this->assertTrue($attributes);
//
//        $attributes = AttributedReflectionClass::methodHasAttributes("getName");
//        $this->assertTrue($attributes);
//
//        $attributes = AttributedReflectionClass::methodHasAttributes("methodWithoutAttributes");
//        $this->assertFalse($attributes);
//
//        $attributes = AttributedReflectionClass::methodHasAttributes("methodWithoutAttributes");
//        $this->assertFalse($attributes);
//
//    }
//
//    public function testMethodsWithAttributes() : void
//    {
//        $attributes = AttributedReflectionClass::methodsWithAttribute(SimpleMethodAttribute::class);
//        $this->assertCount(3, $attributes);
//
//        $attributes = AttributedReflectionClass::methodsWithAttribute(SimpleMethodAttribute::class, false);
//        $this->assertCount(2, $attributes);
//
//        $attributes = AttributedReflectionClass::methodsWithAttribute(NotUsedAttribute::class);
//        $this->assertCount(0, $attributes);
//
//    }
//
//    public function testMethodNamesWithAttributes() : void
//    {
//        $attributes = AttributedReflectionClass::methodNamesWithAttribute(SimpleMethodAttribute::class);
//        $this->assertCount(3, $attributes);
//        $this->assertIsString($attributes[0]);
//
//        $attributes = AttributedReflectionClass::methodNamesWithAttribute(SimpleMethodAttribute::class, false);
//        $this->assertCount(2, $attributes);
//        $this->assertIsString($attributes[0]);
//
//        $attributes = AttributedReflectionClass::methodNamesWithAttribute(NotUsedAttribute::class);
//        $this->assertCount(0, $attributes);
//
//    }
//
//
//}
