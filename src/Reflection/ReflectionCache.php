<?php
/**
 * @author Ryan Spaeth <rspaeth@spaethtech.com>
 * @copyright 2020 - Spaeth Technologies Inc.
 * @package SpaethTech\UCRM\SDK\Reflection
 *
 * @noinspection PhpUnused
 * @noinspection PhpMultipleClassDeclarationsInspection
 */
declare(strict_types=1);

namespace SpaethTech\Reflection;

trait ReflectionCache
{
    /**
     * @var array<string, ReflectionCacheMap>
     */
    protected static array $reflectionCache = [];

    /**
     * @return ReflectionCacheMap
     */
    function getReflectionCache() : ReflectionCacheMap
    {
        $class = get_called_class();

        if (!array_key_exists($class, self::$reflectionCache))
            self::$reflectionCache[$class] = new ReflectionCacheMap($class);

        return self::$reflectionCache[$class];
    }

}
