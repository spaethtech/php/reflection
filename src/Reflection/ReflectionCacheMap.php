<?php
/**
 * @author Ryan Spaeth <rspaeth@spaethtech.com>
 * @copyright 2020 - Spaeth Technologies Inc.
 * @package SpaethTech\UCRM\SDK\Reflection
 *
 * @noinspection PhpUnused
 * @noinspection PhpMultipleClassDeclarationsInspection
 */
declare(strict_types=1);

namespace SpaethTech\Reflection;

use Attribute;
use ReflectionAttribute;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use ReflectionProperty;

class ReflectionCacheMap
{
    #region CONSTANTS

    protected array $ignoredProperties = [
//        "cache",
//        "class",
//        "classAttributes",
//        "properties",
//        "propertyAliases",
//        "methods",
//        "methodAliases",
    ];

    protected array $ignoredMethods = [
//        "getReflectionCache"
    ];

    #endregion

    #region PROPERTIES

    /**
     * @type array<class-string, array{
     *     class: bool,
     *     properties: string[],
     *     methods: string[]
     * }>
     */
    private array $attributeMap = [];

    /**
     * @type ReflectionClass
     */
    private readonly ReflectionClass $class;

    /**
     * @type array<ReflectionAttribute>
     */
    private array $classAttributes = [];

    /**
     * @type array<string, array{
     *     reflected: ReflectionProperty,
     *     attributes: array<ReflectionAttribute>
     * }>
     */
    private array $properties = [];

    /**
     * @type array<string, array{
     *     reflected: ReflectionMethod,
     *     attributes: array<ReflectionAttribute>
     * }>
     */
    private array $methods = [];

    #endregion

    #region CONSTRUCTORS

    public function __construct(
        string $class,
        int $propertyFilters = null,
        int $methodFilters = null
    )
    {
        try
        {
            #region Class

            $this->class = new ReflectionClass($class);
            $this->classAttributes = $this->class->getAttributes();

            foreach($this->classAttributes as $attribute)
            {
                $attributeName = $attribute->getName();

                if (!array_key_exists($attributeName, $this->attributeMap))
                    $this->attributeMap[$attributeName] = [];

                $this->attributeMap[$attributeName]["class"] = true;
            }

            #endregion

            #region PROPERTIES

            foreach($this->class->getProperties($propertyFilters) as $property)
            {
                if (in_array($propertyName = $property->getName(), $this->ignoredProperties))
                    continue;

                $this->properties[$propertyName] = [
                    "reflected" => $property,
                    "attributes" => $property->getAttributes(),
                ];

                foreach($this->properties[$propertyName]["attributes"] as $attribute)
                {
                    $attributeName = $attribute->getName();

                    if (!array_key_exists($attributeName, $this->attributeMap))
                        $this->attributeMap[$attributeName] = [];

                    if (!array_key_exists("properties", $this->attributeMap[$attributeName]))
                        $this->attributeMap[$attributeName]["properties"] = [];

                    $this->attributeMap[$attributeName]["properties"][] = $propertyName;
                }
            }

            #endregion

            #region METHODS

            foreach($this->class->getMethods($methodFilters) as $method)
            {
                if (in_array($methodName = $method->getName(), $this->ignoredMethods))
                    continue;

                $this->methods[$methodName] = [
                    "reflected" => $method,
                    "attributes" => $method->getAttributes(),
                ];

                foreach($this->methods[$methodName]["attributes"] as $attribute)
                {
                    $attributeName = $attribute->getName();

                    if (!array_key_exists($attributeName, $this->attributeMap))
                        $this->attributeMap[$attributeName] = [];

                    if (!array_key_exists("methods", $this->attributeMap[$attributeName]))
                        $this->attributeMap[$attributeName]["methods"] = [];

                    $this->attributeMap[$attributeName]["methods"][] = $methodName;
                }
            }

            #endregion

        }
        catch (ReflectionException)
        {
            // TODO: RuntimeException?
        }

    }

    public static function load(string $path, string $class) : self
    {
        // TODO: Implement

        return new ReflectionCacheMap($class);
    }

    public function save(string $path) : bool
    {
        // TODO: Implement
        return false;
    }

    #endregion

    #region METHODS:CLASS

    /**
     * @return ReflectionClass
     */
    public function getClass() : ReflectionClass
    {
        //$this->buildCache();

        return $this->class;
    }

    /**
     * @return ReflectionAttribute[]
     */
    public function getClassAttributes() : array
    {
        //$this->buildCache();

        return $this->classAttributes;
    }

    /**
     * @param class-string<Attribute> $type
     * @param bool $instance
     * @return Attribute|class-string<Attribute>|null
     */
    public function getClassAttribute(
        string $type,
        bool $instance = false
    ) : Attribute|string|null
    {
        //$this->buildCache();

        $attributes = self::getClassAttributes();

        if(count($attributes) === 0)
            return null;

        foreach($attributes as $attribute)
            if (is_a($attribute->getName(), $type, true))
                return $instance ? $attribute->newInstance() : $attribute;

        return null;
    }

    #endregion

    #region METHODS:PROPERTIES

    /**
     * @return array<string, array{
     *      reflected: ReflectionProperty,
     *      attributes: array<ReflectionAttribute>
     *  }>
     */
    public function getProperties() : array
    {
        return $this->properties;
    }

    /**
     * @param string $name The property name
     * @return array{
     *       reflected: ReflectionProperty,
     *       attributes: array<ReflectionAttribute>
     *   }|null
     */
    public function getProperty(string $name) : array|null
    {
        if (!array_key_exists($name, $this->properties))
            return null;

        return $this->properties[$name];
    }

    /**
     * @param string $name The property name
     * @param bool $instance TRUE returns the Attribute instance, FALSE the name
     *
     * @return Attribute[]|ReflectionAttribute[]|null
     */
    public function getPropertyAttributes(string $name, bool $instance = false) : array|null
    {
        if (!array_key_exists($name, $this->properties))
            return null;

        $attributes = $this->properties[$name]["attributes"];

        return $instance
            ? array_map(fn($a) =>$a->newInstance(), $attributes)
            : $attributes;
    }

    /**
     * @param string $name The property name
     * @param class-string<Attribute> $type The Attribute type
     * @param bool $instance TRUE returns the Attribute instance, FALSE the name
     * @return Attribute|class-string<Attribute>|null
     */
    public function getPropertyAttribute(
        string $name,
        string $type,
        bool $instance = false
    ) : Attribute|string|null
    {
        $attributes = $this->getPropertyAttributes($name);

        if($attributes === null)
            return null;

        foreach($attributes as $attribute)
            if (is_a($attribute->getName(), $type, true))
                return $instance ? $attribute->newInstance() : $attribute;

        return null;
    }

    /**
     * @param class-string<Attribute> $type
     * @return string[]
     */
    public function getPropertiesWithAttribute(string $type): array
    {
        return $this->attributeMap[$type]["properties"] ?? [];
    }



    #endregion








    public function getMethods() : array
    {
        //$this->buildCache();
        return $this->methods;
    }


    public function getMethod(string $name) : ?array
    {
        $methods = $this->getMethods();

        if (!array_key_exists($name, $methods))
            return null;

        return $methods[$name];
    }

    public function getMethodAttributes(string $name) : ?array
    {
        $method = $this->getMethod($name);

        if($method === null)
            return null;

        return $method["attributes"];
    }

    public function getMethodAttribute(string $name, string $type) : ?Attribute
    {
        $attributes = $this->getMethodAttributes($name);

        if($attributes === null)
            return null;

        foreach($attributes as $attribute)
            if (is_a($attribute, $type))
                return $attribute;

        return null;
    }

}
