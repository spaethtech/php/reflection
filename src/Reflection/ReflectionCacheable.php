<?php
/**
 * @author Ryan Spaeth <rspaeth@spaethtech.com>
 * @copyright 2020 - Spaeth Technologies Inc.
 * @package SpaethTech\UCRM\SDK\Reflection
 *
 * @noinspection PhpUnused
 * @noinspection PhpMultipleClassDeclarationsInspection
 */
declare(strict_types=1);

namespace SpaethTech\Reflection;

interface ReflectionCacheable
{
    /**
     * @return ReflectionCacheMap
     */
    function getReflectionCache() : ReflectionCacheMap;
}
